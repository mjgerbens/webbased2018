<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 22-11-18
  Time: 9:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Log in</title>
</head>
    <body>

    <c:choose>
        <c:when test="${sessionScope.user != null}">
            <h1>Welcome, ${sessionScope.user.userName}</h1>

            <form action="logout.do" method="POST">
                <input type="submit" value="Log out" />
            </form>
        </c:when>
        <c:otherwise>
            <h2>please log in first</h2>
            <h4>${requestScope.errorMessage}</h4>
            <form action="login.do" method="POST">
                <label for="username_field"> User name: </label>
                <input id="username_field" type="text" name="username" required/> <br/>
                <label for="password_field"> User password: </label>
                <input id="password_field" type="password" name="password" required/><br/>
                <label class="login_field"> </label>
                <input type="submit" value="Log in"/>
            </form>
        </c:otherwise>
    </c:choose>
    </body>
</html>
