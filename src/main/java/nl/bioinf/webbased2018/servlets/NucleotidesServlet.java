package nl.bioinf.webbased2018.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "NucleotidesServlet", urlPatterns = "/nucleotides")
public class NucleotidesServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> nucleotides = new HashMap<>();
        nucleotides.put("A", "Adenine");
        nucleotides.put("C", "Cytosine");
        nucleotides.put("G", "Guanine");
        nucleotides.put("T", "Thymine");

        request.setAttribute("nucleotides", nucleotides);
        RequestDispatcher view = request.getRequestDispatcher("nucleotides.jsp");
        view.forward(request, response);
    }
}
